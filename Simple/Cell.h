#include "Texture.h"
#include "Drawable.h"

#ifndef __CELL__H__
#define __CELL__H__

class Cell: public Drawable {
public:
    int texture;

    Cell(): texture(-1) { }

    virtual int width() const { return 20; }
    virtual int hight() const { return 20; }
    virtual void draw(Environment &env, Rendered &w, SDL_Rect &pos);
};

#endif