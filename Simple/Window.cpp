#include "Window.h"

Window::Window(int width, int height, Environment *env): _renderer(nullptr) {
    _width = width;
    _height = height;
    _env = env;
}


Window::~Window() {
    SDL_DestroyWindow(_window);
    _env->log("Window destroyed.");
}


bool Window::open(const char *title) {
    _env->log("Window openning...");
    if (_env->init()) {
        _window = SDL_CreateWindow(title, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, _width, _height, SDL_WINDOW_SHOWN);
        if (_window == nullptr) {
            _env->log("Window openning fail. SDL_CreateWindow error.");
            return false;
        } else {
            _env->log("Window oppenning successfull.");
            _renderer = nullptr;
            return true;
        }
    } else {
        _env->log("Window oppening is fail.");
        return false;
    }
}


SDL_Renderer* Window::getRenderer() {
    if (!_env->isInited()) { 
        return nullptr; 
    }

    if (_renderer == nullptr) {
        _renderer = SDL_CreateRenderer(_window, -1, SDL_RENDERER_ACCELERATED|SDL_RENDERER_PRESENTVSYNC);
    }

    return _renderer;    
}