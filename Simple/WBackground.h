#include <SDL.h>
#include "Drawable.h"
#include "Utils.h"


#ifndef __W_BACKGROUND__H__
#define __W_BACKGROUND__H__

class WBackground: Drawable {
private:
    int _background_texture_id;
    Drawable *_nested;

public:
    WBackground(int background_texture_id, SDL_Rect pos, Drawable *nested = nullptr);
    ~WBackground();
    virtual void draw(Environment &env, Rendered &w, SDL_Rect &pos);
};

#endif