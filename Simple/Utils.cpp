#include "Utils.h"

SDL_Rect* make_rect(int x, int y, int w, int h) {
    SDL_Rect *r = new SDL_Rect();

    r->x = x;
    r->y = y;
    r->w = w;
    r->h = h;

    return r;
}

SDL_Rect combine_rect(const SDL_Rect &frame, const SDL_Rect &size) {
    SDL_Rect res(frame);

    if (size.h < res.h) {
        res.h = size.h;
    }

    if (size.w < res.w) {
        res.w = size.w;
    }

    return res;
}