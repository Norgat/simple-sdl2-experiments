#include "Cell.h"


void Cell::draw(Environment &env, Rendered &w, SDL_Rect &pos) {    
    if (pos.w > width()) {
        pos.w = width();
    }

    if (pos.h > hight()) {
        pos.h = hight();
    }

    w.renderDraw(*env.textures[texture], pos);
}