#include "Rendered.h"


Texture* Rendered::renderSurface(Surface &surface) {
    return Texture::makeTexture(SDL_CreateTextureFromSurface(getRenderer(), surface));
}


void Rendered::renderClear() {
    SDL_RenderClear(getRenderer());
}


void Rendered::renderDraw(Texture &texture) {
    renderDraw(texture, nullptr, nullptr);
}


void Rendered::renderDraw(Texture &texture, SDL_Rect &pos) {
    SDL_Rect src_pos(pos);
    src_pos.x = 0;
    src_pos.y = 0;

    renderDraw(texture, &src_pos, &pos);
}


void Rendered::renderDraw(Texture &texture, SDL_Rect &source, SDL_Rect &dist) {
    renderDraw(texture, &source, &dist);
}


void Rendered::renderPresent() {
    SDL_RenderPresent(getRenderer());
}


inline
void Rendered::renderDraw(Texture &texture, SDL_Rect *dist, SDL_Rect *source) {
    SDL_RenderCopy(getRenderer(), texture, dist, source);
}
