#include "TextureManager.h"


TextureManager::~TextureManager() {
    for (size_t i = 0; i < _textures.size(); ++i) {
        delete _textures[i];
    }
}


Texture* TextureManager::operator[] (int id) {
    std::map<int, Texture*>::iterator i = _table.find(id);

    if (i == _table.end()) {
        return nullptr;
    }

    return i->second;
}


void TextureManager::addTexture(int id, Texture *tex) {
    _textures.push_back(tex);
    _table[id] = tex;
}