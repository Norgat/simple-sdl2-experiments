#include "W22.h"


void W22::draw(Environment &env, Rendered &w, SDL_Rect &pos) {

    for (int i = 0; i < 2; ++i) {
        for (int j = 0; j < 2; ++j) {
            SDL_Rect cell_pos(pos);
            cell_pos.x += i*25;
            cell_pos.y += j*25;

            _cells[i][j].draw(env, w, cell_pos);
        }
    }
}