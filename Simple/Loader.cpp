#include "Loader.h"


Surface* Loader::loadBMP(const char *path) {
    return Surface::makeSurface(SDL_LoadBMP(path));
}


void Loader::loadTexture(Environment &env, Rendered &w, int id, const char *path) {
    Surface *s = Surface::makeSurface(SDL_LoadBMP(path));
    env.textures.addTexture(id, w.renderSurface(*s));
    delete s;
}