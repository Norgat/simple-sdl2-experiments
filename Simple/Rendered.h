#include <SDL.h>
#include "Surface.h"
#include "Texture.h"

#ifndef __RENDERED__H__
#define __RENDERED__H__

class Rendered {
private:
    inline void renderDraw(Texture &texture, SDL_Rect *source, SDL_Rect *dist);

public:
    virtual SDL_Renderer *getRenderer() = 0;

    Texture* renderSurface(Surface &surface);

    void renderClear();
    void renderDraw(Texture &texture);
    void renderDraw(Texture &texture, SDL_Rect &pos);
    void renderDraw(Texture &texture, SDL_Rect &dist, SDL_Rect &source);
    void renderPresent();
};

#endif