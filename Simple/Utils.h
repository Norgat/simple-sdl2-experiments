#include <SDL.h>

#ifndef __UTILS__H__
#define __UTILS__H__


SDL_Rect* make_rect(int x, int y, int w, int h);
SDL_Rect combine_rect(const SDL_Rect &frame, const SDL_Rect &size);

#endif