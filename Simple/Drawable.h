#include <SDL.h>
#include "Env.h"
#include "Rendered.h"

#ifndef __DRAWABLE__H__
#define __DRAWABLE__H__

class Drawable {
protected:
    SDL_Rect _pos;

public:
    virtual int width() const { return _pos.w; }
    virtual int hight() const { return _pos.h; }

    virtual int x() const { return _pos.x; }
    virtual int y() const { return _pos.y; }

    virtual SDL_Rect position() { return SDL_Rect(_pos); }
    virtual void updatePos(SDL_Rect pos) { _pos = pos; }

    virtual void draw(Environment &env, Rendered &w, SDL_Rect &pos) = 0;
};

#endif