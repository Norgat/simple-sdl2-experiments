#include <SDL.h>

#include "Env.h"
#include "Texture.h"
#include "Drawable.h"
#include "Cell.h"


#ifndef __W22__H__
#define __W22__H__

class W22: public Drawable {
private:
    Cell **_cells;

public:
    W22(Cell *cells[]) {
        _cells = cells;
    }

    virtual int width() const { return 100; }
    virtual int hight() const { return 100; }

    virtual void draw(Environment &env, Rendered &w, SDL_Rect &pos);    
};

#endif