#include <SDL.h>
#include "Env.h"
#include "Rendered.h"


#ifndef __WINDOW__H__
#define __WINDOW__H__

class Window: public Rendered {
private:
    int _width;
    int _height;

    Environment *_env;

    SDL_Window *_window;
    SDL_Renderer *_renderer;

public:
    Window(int width, int height, Environment *env);
    ~Window();
    
    bool open(const char *title);

    virtual SDL_Renderer* getRenderer();
};

#endif