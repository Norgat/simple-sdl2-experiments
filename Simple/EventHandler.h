#include <SDL.h>

#ifndef __EVENT_HANDLER__H__
#define __EVENT_HANDLER__H__

class EventHandler {
public:
    virtual void handle(SDL_Event *evn) = 0;
};

#endif