#include <SDL.h>

#ifndef __SURFACE__H__
#define __SURFACE__H__

class Surface {
private:
    SDL_Surface *_surface;
    Surface(SDL_Surface *surface): _surface(surface) { }

public:
    static Surface* makeSurface(SDL_Surface *surface) {
        if (surface == nullptr) { 
            return nullptr;
        }

        return new Surface(surface);
    }

    operator SDL_Surface* () { return _surface; }

    ~Surface() {
        SDL_FreeSurface(_surface);
    }
};

#endif