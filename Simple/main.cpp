#include <iostream>
#include <SDL.h>

#include "Env.h"
#include "Window.h"
#include "Loader.h"
#include "Surface.h"
#include "Texture.h"
#include "Utils.h"
#include "EventHandler.h"
#include "W22.h"
#include "Cell.h"
#include "WBackground.h"

#include <map>


class MouseDownHandler: public EventHandler {
private:
    Environment *_env;

public:
    MouseDownHandler(Environment *env) {
        _env = env;
    }


    virtual void handle(SDL_Event *evn) {
        _env->exit();
    }
};


int main(int argc, char **argv) {
    Environment *env = new Environment();
    env->init();

    Window *w = new Window(640, 480, env);
    w->open("Hi!");

    Loader::loadTexture(*env, *w, 1, "D:/tex.bmp");
    Loader::loadTexture(*env, *w, 2, "D:/background.bmp");
    Loader::loadTexture(*env, *w, 3, "D:/cell.bmp");
    Loader::loadTexture(*env, *w, 4, "D:/filled_cell.bmp");


    std::map<Uint32, EventHandler*> event_handler;
    event_handler[SDL_MOUSEBUTTONDOWN] = new MouseDownHandler(env);

    Cell *cells[2];
    for (int i = 0; i < 2; ++i) {
        cells[i] = new Cell[2];
        for (int j = 0; j < 2; ++j) {
            cells[i][j].texture = 1;
        }
    }

    W22 *widget = new W22(cells);

    SDL_Rect null_pos;
    null_pos.x = 0;
    null_pos.y = 0;
    null_pos.w = 640;
    null_pos.h = 480;

    WBackground background(2, null_pos, widget);
    
    
    SDL_Event e;
    while (!env->isQuited()) {

        while (SDL_PollEvent(&e)) {
            std::map<Uint32, EventHandler*>::iterator i = event_handler.find(e.type);
            if (i!= event_handler.end()) {
                i->second->handle(&e);
            }
        }

        w->renderClear();
        background.draw(*env, *w, null_pos);
        w->renderPresent();
    }

    delete w;
    delete env;

    std::cout << "Press ENTER to exit..." << std::endl;
    std::cin.get();

	return 0;
}