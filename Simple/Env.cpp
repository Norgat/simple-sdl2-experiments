#include "Env.h"


Environment::Environment(std::ostream *stream): 
    _initialized(false), _quit(false) {
    _stream = stream;
}


Environment::~Environment() {
    deinit();
}


bool Environment::init() {
    if (!_initialized) {  
        log("SDL initialization started...");
        if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {            
            log("SDL init failed");
            _initialized = false;
        } else {
            log("SDL init succesfull");
            _initialized = true;
        }
    }

    return _initialized;
}


void Environment::deinit() {
    if (_initialized) {
        SDL_Quit();
        _initialized = false;
        log("SDL deinit");
    }
}


void Environment::log(const char *msg) const {
    (*_stream) << msg << std::endl;
}


bool Environment::isInited() const { return _initialized; }