#include <iostream>
#include <map>
#include <SDL.h>
#include "Texture.h"
#include "TextureManager.h"

#ifndef __ENV__H__
#define __ENV__H__

class Environment {
private:
    bool _initialized;
    std::ostream *_stream;

    bool _quit;    
    

public:
    Environment(std::ostream *stream = &(std::cout));
    ~Environment();

    bool init();
    void deinit();
    bool isInited() const;

    void log(const char *msg) const;

    bool isQuited() const { return _quit; }
    void exit() { _quit = true; }
           
    TextureManager textures;
};

#endif