#include <SDL.h>

#ifndef __TEXTURE__H__
#define __TEXTURE__H__

class Texture {
private:
    SDL_Texture *_texture;
    Texture(SDL_Texture *texture): _texture(texture) { }

public:
    static Texture* makeTexture(SDL_Texture *texture) {
        if (texture == nullptr) { 
            return nullptr; 
        }

        return new Texture(texture);
    }

    
    operator SDL_Texture* () { return _texture; }


    ~Texture() {
        SDL_DestroyTexture(_texture);
    }
};

#endif