#include <SDL.h>
#include "Surface.h"
#include "Rendered.h"
#include "Env.h"

#ifndef __LOADER__H__
#define __LOADER__H__

class Loader {
private:
    Loader() { };

public:
    static Surface* loadBMP(const char *path);
    static void loadTexture(Environment &env, Rendered &w, int id, const char *path);
};

#endif