#include <SDL.h>
#include "Texture.h"
#include <vector>
#include <map>


#ifndef __TEXTURE_MANAGER__H__
#define __TEXTURE_MANAGER__H__

class TextureManager {
private:

    // pointers to ALL loaded textured stored here
    // it needed for memory management
    std::vector<Texture*> _textures;

    // map for translation TextureId to Texture
    std::map<int, Texture*> _table;

public:
    TextureManager() { }
    ~TextureManager();

    Texture* operator[] (int id);
    
    void addTexture(int id, Texture *tex);

    int count() const { _textures.size(); }
};

#endif