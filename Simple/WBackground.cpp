#include "WBackground.h"


WBackground::WBackground(int background_texture_id, SDL_Rect pos, Drawable *nested) {
    _background_texture_id = background_texture_id;
    _pos = pos;
    _nested = nested;
}


void WBackground::draw(Environment &env, Rendered &w, SDL_Rect &pos) {
    w.renderDraw(*env.textures[_background_texture_id], combine_rect(pos, _pos));
    if (_nested) {
        _nested->draw(env, w, pos);
    }
}


WBackground::~WBackground() {
    delete _nested;
}